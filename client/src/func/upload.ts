import * as HttpHandler from "./http";

export function getFiles() : Promise<HttpHandler.Response> {
    return new Promise((resolve, reject) => {
        fetch(HttpHandler.UPLOAD_URL, {
            method: "get",
        }).then(res => {
            res.json().then((resData: HttpHandler.Response) => {
                resolve(resData);
            });
        }).catch(err => {
            reject({ error: err, data: null });
        });
    });
}

export function deleteFile(name: string) : Promise<HttpHandler.Response> {
    return new Promise((resolve, reject) => {
        fetch(HttpHandler.UPLOAD_URL, {
            method: "delete",
            body: JSON.stringify({ name: name }),
            headers: {
                "Content-Type": "application/json"
            },
        }).then(res => {
            res.json().then((resData: HttpHandler.Response) => {
                resolve(resData);
            });
        }).catch(err => {
            reject({ error: err , data: null });
        });
    });
}

export function uploadFile(data: FormData) : Promise<HttpHandler.Response>  {
    return new Promise((resolve, reject) => {
        fetch(HttpHandler.UPLOAD_URL, {
            method: "post",
            body: data
        }).then(res => {
            res.json().then((resData: HttpHandler.Response) => {
                resolve(resData);
            });
        }).catch(err => {
            reject({ error: err , data: null });
        });
    });
}
