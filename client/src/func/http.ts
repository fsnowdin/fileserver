export const BASE_URL = "";

export const UPLOAD_URL = BASE_URL + "/api/upload";

export interface Response {
    error: string;
    // eslint-disable-next-line
    data: any;
}
