FROM node:lts-alpine as client

WORKDIR /opt/fileserver/client

COPY client/package*.json ./
COPY client/tsconfig.json ./
COPY client/babel.config.js ./
COPY client/.browserslistrc ./

RUN npm install

COPY client .

RUN npm run build

FROM golang:1.17-alpine

WORKDIR /opt/fileserver/client

COPY --from=client /opt/fileserver/client .

WORKDIR /opt/fileserver/server

COPY server .

RUN go get -d -v ./...
RUN go install -v ./...

EXPOSE 8080

WORKDIR /opt/fileserver/server

CMD [ "go", "run", "src/main.go" ]
